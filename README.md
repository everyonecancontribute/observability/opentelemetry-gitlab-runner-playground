# OpenTelemetry GitLab Research

## Research Scope

"Research OpenTelemetry with Tracing to help achieve CI/CD Observability in GitLab."

- CI/CD Observability with OpenTelemetry feature proposal: https://gitlab.com/gitlab-org/gitlab/-/issues/338943
- FY23 Observability strategy: https://gitlab.com/groups/gitlab-com/marketing/-/epics/2593

## Purpose

Collect insights and ideas, and document the contribution experience with GitLab and OpenTelemetry. This research project consists of multiple paths and learnings.

Owner: [@dnsmichi](https://gitlab.com/dnsmichi)

## Documentation

Inside [docs/](docs/), deployed using GitLab Pages to https://gitlab.opentelemetry.love 


